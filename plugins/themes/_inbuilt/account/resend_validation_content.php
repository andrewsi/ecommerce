<div id="maincontent">
    <p>Enter your email address, and we'll send you a new validation code</p>
    <h2>Resend Validation Email</h2>

    <?php

    print_message('error_message', $session->get_error_message());

    ?>

    <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
        <input type="hidden" name="submitted" value="1">
        <input type="hidden" name="page" value="account">
        <input type="hidden" name="action" value="resendValidation">
       <p>
            Email address: <input type="text" name="email" value="<?php print_safe('email', $_REQUEST) ?>">
        </p>

        <p><input type="Submit" value="Resend Email"></p>
    </form>


</div>
