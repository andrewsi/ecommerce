<div id="maincontent">
    <p>Enter your new password</p>

    <?php

    print_message('error_message', $session->get_error_message());

    ?>

    <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
        <input type="hidden" name="submitted" value="2">
        <input type="hidden" name="page" value="account">
        <input type="hidden" name="action" value="resetPassword">
        <p>
            New password: <input type="password" name="password_1" value="">
        </p>
        <p>
            New password (again): <input type="password" name="password_2" value="">
        </p>

        <p><input type="Submit" value="Update password"></p>
    </form>
</div>
