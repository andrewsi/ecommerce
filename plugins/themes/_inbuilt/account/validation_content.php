<div id="maincontent">
    <p>Enter your validation code</p>

    <?php

    print_message('error_message', $session->get_error_message());

    ?>

    <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
        <input type="hidden" name="submitted" value="1">
        <input type="hidden" name="page" value="account">
        <input type="hidden" name="action" value="validate">
        <p>
            Validation code: <input type="text" name="validation_code" value="">
        </p>

        <p><input type="Submit" value="Validate account"></p>
    </form>
</div>
