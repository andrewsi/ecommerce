<?php

print_message('error_message', $session->get_error_message());

?>

<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
    <input type="hidden" name="action" value="doLogin">
    <p>
        Username: <input type="text" name="username" value="<?php print_safe('username', $_REQUEST) ?>">
    </p>
    <p>
        Password: <input type="password" name="password">
    </p>

    <p><input type="Submit" value="Log in"></p>
    <p><a href="<?php echo get_link_url(['page' =>  'account', 'action' => 'forgottenPassword' ]) ?>">Forgotten Your Password?</a></p>
    <p><a href="<?php echo get_link_url(['page' =>  'account', 'action' => 'create' ]) ?>">Create Account</a></p>
</form>

