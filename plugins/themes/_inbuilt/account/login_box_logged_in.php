<div>
    <h3>Your account</h3>

    <p><a href="<?php echo get_link_url(['page' => 'viewOrders']) ?>">See your orders</a></p>

    <p><a href="<?php echo get_link_url(['page' => 'viewAccount']) ?>">Change your account settings</a></p>

    <p><a href="<?php echo get_link_url(['action' =>  'logOut']) ?>">Log Out</a></p>
</div>