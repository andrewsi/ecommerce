<?php

print_message('error_message', $session->get_error_message());

?>

<p>You have not yet validated your account.</p>

<p><a href="<?php print_link_url(['page' =>  'account', 'action' => 'resendValidation' ]) ?>">Resend the validation email</a></p>
<p><a href="<?php print_link_url(['page' =>  'account', 'action' => 'validate' ]) ?>">Enter the validation code</a></p>
