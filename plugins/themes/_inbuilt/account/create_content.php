<div id="maincontent">
<h2>Create your account</h2>

<?php

print_message('error_message', $session->get_error_message());

?>

<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
    <input type="hidden" name="submitted" value="1">
    <input type="hidden" name="page" value="account">
    <input type="hidden" name="action" value="create">
    <p>
        Username: <input type="text" name="username" value="<?php print_safe('username', $_REQUEST) ?>">
    </p>
    <p>
        Email address: <input type="text" name="email" value="<?php print_safe('email', $_REQUEST) ?>">
    </p>
    <p>
        Password: <input type="password" name="password_1">
    </p>
    <p>
        Password (again): <input type="password" name="password_2">
    </p>

    <p><input type="Submit" value="Create Account"></p>
</form>


</div>
