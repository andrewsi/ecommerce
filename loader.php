<?php

/*
 *	This is where the work is actually done
 *
 *	We load all the files we're going to need, and set up everything else that will be required.
 */

require (dirname(__FILE__) . '/config.php');

// Create the database connection
$DB = new DB();

$config = Config::getInstance();

// Set up constants so I can refer to files without having to retype the path every time.
define ('THEMEROOT', FILEROOT . 'plugins/themes/' . $config->theme_name . '/');
define ('CODEROOT', FILEROOT . 'code/');
define ('PAGEROOT', CODEROOT . 'pages/');

// Load the files that provide the functionality we're going to use
// Files loaded provide core functionality
require (CODEROOT . 'triggers.php');
require (CODEROOT . 'functions_core.php');
require (CODEROOT . 'navigation.php');

/* Load the constants we're going to be using.
 * We've set a few above; but these are the ones that are stored in the database.
 * If it's in a list_xx_status table, then it should be added here.
 * This will load the values - they'll be prefixed with the table name, and the value is the value from the database
 * So we'll get SESSION_LOGGED_IN as a constant.
 */
$table_names = ['order', 'session', 'user'];
load_constants($table_names);

// Themes require some files. Make sure that the theme we're using actually has them
$required_files = array('header', 'footer', 'product', 'search', 'account', 'category');

# Check that we have a HTML file for every required_page
foreach ($required_files as $file) {
    if (! file_exists(THEMEROOT . $file . '.php')) {
        log_error("Unable to load $file.php");
    }
}
// Themes have some optional pages, too; check to see if these exist, and load them if they do.
$optional_files = array('functions', 'filters');

foreach ($optional_files as $file) {
    if (file_exists(THEMEROOT . $file . '.php')) {
        require (THEMEROOT . $file . '.php');
    }
}

$pages = ['account'];

$switchboard = Switchboard::getInstance();

foreach ($pages as $page) {
    include(CODEROOT . 'pages/' . $page . '.php');
}

// Load the rest of the main files.
// Files loaded at this point provide functionality that might have been over-ridden by theme files
require (CODEROOT . 'functions.php');
require (CODEROOT . 'filters.php');

# Now we have everything configured, we can figure out what we want to show.
# First thing we do is check to see if we're logged in.
check_login();

# Set the User and UserSession instances.
# These may have already been created and populated in the check_login function
$user = User::getInstance();
$session = UserSession::getInstance();

$basket = Basket::getInstance();
if (! $basket->loadFromDB([ 'session_id' => $session->session_id])) {
    $basket->session_id = $session->session_id;
}


if (isset($_REQUEST['page']) && $switchboard->page_exists($_REQUEST['page'])) {
    call_user_func($switchboard->get_file($_REQUEST['page']));
} else {
    include(CODEROOT . 'pages/main.php');
}

