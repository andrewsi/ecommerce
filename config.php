<?php

/*
 * includes basic configuration setup, plus the autoloader
 */

function class_autoloader ($class) {
	require (FILEROOT . 'code/class.' . $class . '.php');
}

// database credentials here

define ('DB_TYPE', 'mysql');
define ('DB_HOSTNAME', 'localhost');
define ('DB_DATABASENAME', 'ecommerce');
define ('DB_USERNAME', 'ecommerce_user');
define ('DB_PASSWORD', 'password123');

// where this installation is located

define ('FILEROOT' , dirname(__FILE__) . '/');

spl_autoload_register ('class_autoloader');
