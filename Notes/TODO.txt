*   I've split the account code down into something that's going to be more easily managed, as well as moving things
    out of UserSession that really didn't need to be in there.

    Now I'm thinking that I maybe want to re-do it again - look at putting everything into classes, one way or another,
    so I don't have to worry about namespace collisions.

*   Grrr. We're now not getting the session being set on the first pageload. Fix that!