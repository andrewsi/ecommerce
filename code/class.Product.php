<?php

/*
 * This is one I've spent way too much time thinking about.
 *
 * A Product isn't just an item; it's also a snapshot of that item at a point in time. If I come back to a site
 * a month later and the price has gone up, I need to know that this item is available now at the new price, but
 * also have it showing in my order history with the old item.
 *
 * So a Product is made up of two parts. There's:
 *
 * ProductDetails
 * ProductInstance
 *
 * ProductDetails stores all the information about a product that aren't going to change - the physical size, and so
 * on.
 *
 * ProductInstance is the details that will change; the price and the SKU, for example.
 *
 * Changing the price or SKU of a Product will create a new ProductInstance, but not a new ProductDetails.
 */