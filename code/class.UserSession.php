<?php

/*
 * Code to store details of a logged in user's session.
 */

final class UserSession extends Factory {
    protected $table_name = 'user_session';
    protected $primary_key = 'user_session_id';
    protected $ID = NULL;

    private static $instance = NULL;

    private $error_message = '';

    public function set_error_message ($message) {
        $this->error_message = $message;
    }

    public function get_error_message () {
        return ($this->error_message);
    }

    public static function getInstance() {
        if (self::$instance == NULL) {
            self::$instance = new UserSession();
        }

        return self::$instance;
    }

    # Create our user session object; there's one assigned to each user session.
    private function __construct() {
        $this->parameters = array_fill_keys(array(
            'user_id',
            'ip_address',
            'session_status_id',
            'session_id',
        ), NULL);
    }

    public function is_logged_in() {
        if ($this->session_status_id == SESSION_LOGGED_IN) {
            return TRUE;
        }

        return FALSE;
    }

    public function set_login_status($new_status) {
        $this->session_status_id = $new_status;
    }
}

