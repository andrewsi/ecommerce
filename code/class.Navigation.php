<?php

/*
 *	Creates a Navigation object
 *
 *	This is a simplified way to generate the navigation menu.
 *	Each object will contain all the details from the Category table, as well as an array (that might be empty)
 *	with the IDs of all the objects that have this as a parent (ie children). That should make iterating through 
 *	the navigation a lot simpler.
 *
 *	Note that this doesn't extend Factory - it gets populated directly in the navigation code
 *
 */

Class Navigation {
	// These are the fields that are populated directly in the constructor, and are compulsory

	private $category_id;
	private $category_name;
	private $parent_id;
	private $children;

	// These are the optional fields
	// As per normal, I'm using a parameter hash, rather than a set of named parameters

	private $parameters = array();

	public function __construct($parameters) {
		$this->category_id = $parameters['category_id'];
		$this->category_name = $parameters['category_name'];
		$this->parent_id = $parameters['parent_id'];
		$this->children = array();
	}

	public function addChild ($child) {
		array_push ($this->children, $child);
	}

	public function getCategoryId() {
		return $this->category_id;
	}

	public function getCategoryName() {
		return $this->category_name;
	}

	public function getParentID() {
		return $this->parent_id;
	}

	public function getChildren() {
		return $this->children;
	}
}
