<?php

/*
 * This is an abstract class.
 *
 * It's a wrapper around a hash, with methods to add, delete, and retrieve values.
 *
 * It's designed to be used to create a static instance, which can then be passed around more easily - you can reference a static instance anywhere, so 
 * there's no need to pass it into a function as a parameter or as a global. This seems like a reasonable idea right now.
 *
 * Note that this is abstract - you'll need to extend it before you can use it, though that means that you can also easily add extra functionality if 
 * it's required. The main use for this is so you can extend the class, and add an `initialize()` method in it, so that you can create 
 * default options for any parameters you want to.
 * 
 */

abstract class parameters {
	static private $parameters = array();
	static private $initialized = false;

	static public function setParameter($key, $value) {
		/*
		 * Set a parameter. 
		 * Doesn't check to see if the parameter is already set, so it'll over-write one if it's there already.
		 *
		 * Checks to see if this is the first time we're using this particular instance. If so, it'll run an initialize() method if there is one - 
		 * that's how I'm faking a construct call on a static object
		 */
		self::$parameters[$key] = $value;

		if (! self::$initialized) {
			if (method_exists(static::class, 'initialize')) {
				self::$initialized = true;
				static::initialize();
			}
		}
	}

	static public function setParameters ($parameters) {
		/*
		 * Set multiple parameters
		 *
		 * Expects an array of parameters (and will ignore any other input.)
		 * Steps through the array, and adds each one using the setParameter function.
		 */

		$parameterSetCount = 0;

		if (is_array($parameters)) {
			foreach ($parameters as $key => $value) {
				$parameterSetCount++;
				self::setParameter($key, $value);
			}
		}

		return $parameterSetCount;
	}

	static public function deleteParameter ($key) {
		/*
		 * Deletes a parameter
		 *
		 * Checks to see if the parameter exists first; returns a boolean true / false if the parameter was there to 
		 * be deleted
		 *
		 */
		if (isset(self::$parameters[$key])) {
			unset (self::$parameters[$key]);
			return true;
		}

		return false;
	}

	static public function getParameter ($key) {
		/*
		 * Returns a single parameter, using the key passed in as a parameter
		 *
		 * Returns an empty string if there's no such key in the hash
		 */

		if (isset(self::$parameters[$key])) {
			return self::$parameters[$key];
		}

		return "";
	}
}
