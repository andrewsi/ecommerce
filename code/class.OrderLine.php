<?php

/*
 * Works with Order to define an order; this is the details of a specific item that is in the order.
 *
 * An OrderLine has a link to an order_id
 * It has a link to one product_id; it contains the quantity.
 *
 * The price is calculated dynamically from the above.
 */

final class OrderLine extends Factory {
    protected $table_name = 'order_line';
    protected $primary_key = 'order_line_id';
    protected $ID = NULL;

    protected $product_id = NULL;
    protected $quantity = NULL;

    protected $order_id = NULL;

    public function __construct($DB, $order_id, $product_id, $quantity) {
        $this->DB = $DB;

        $this->product_id = $product_id;
        $this->quantity = $quantity;

        $this->order_id = $order_id;
    }

    public function change_quantity($quantity) {
        $this->quantity = $quantity;
    }
}