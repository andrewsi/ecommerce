<?php

final class DB extends PDO
{
    private $dbtype;
    private $host;
    private $database;
    private $user;
    private $password;

    private $status = 'disconnected';

    private static $instance = NULL;

    public function __construct() {
        $this->dbtype = DB_TYPE;
        $this->host = DB_HOSTNAME;
        $this->database = DB_DATABASENAME;
        $this->user = DB_USERNAME;
        $this->password = DB_PASSWORD;

        $dsn = $this->dbtype . ":host=" . $this->host . ";dbname=" . $this->database;

        try {
            parent::__construct($dsn, $this->user, $this->password);
            $this->status = 'connected';
        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
            $this->status = 'failed to connect';
        }
    }

    public static function getInstance() {
        if (self::$instance == NULL) {
            self::$instance = new DB();
        }

        return self::$instance;
    }
}
