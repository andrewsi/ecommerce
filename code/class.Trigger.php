<?php

class trigger {
	##
	# Class for the details of a given trigger; the only properties are the function name and the parameters to pass in.
	##

	private $functionName;
	private $parameters;

	public function __construct($functionName, $parameters = array()) {
		# Public constructor.

		$this->functionName = $functionName;
		$this->parameters = $parameters;
	}

	public function getFunctionName() {
		## Getter for the function name

		return $this->functionName;
	}

	public function getParameters() {
		## Getter for the parameters 

		return $this->parameters;
	}
}

