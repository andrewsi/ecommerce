<?php

/*
 * All the code that deals with Users goes here.
 *
 * I'm rolling my own authentication system; there's literally no way that this could go wrong.
 *
 * All the authentication is done on the server; when someone logs in, they're given a session_id, and we're
 * storing their login status in a table on the server, using that as the key.
 *
 */


final class User extends Factory {
    protected $table_name = 'user';
    protected $primary_key = 'user_id';
    protected $ID = null;

    private static $instance = NULL;

    protected $access_level;

    private function __construct() {
        $this->parameters = array_fill_keys(array(
            'username',
            'password',
            'email_address',
            'user_status_id',
            'validation_code',
        ), '');

        return TRUE;
    }

    public static function getInstance() {
        if (self::$instance == NULL) {
            self::$instance = new User();
        }

        return self::$instance;
    }

    # Check that a password change is correct; compare the two passwords and make sure they're the same, and that
    # the old password matches the existing one.
    public function change_password($password_1, $password_2, $old_password) {
        if ($password_1 !== $password_2) {
            return 1;
        }

        if (! $this->check_password($old_password)) {
            return 2;
        }

        $this->password = password_hash($password_1, PASSWORD_DEFAULT);

        return 0;
    }
}
