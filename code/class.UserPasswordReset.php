<?php

final class UserPasswordReset extends Factory
{
    protected $table_name = 'user_password_reset';
    protected $primary_key = 'user_password_reset_id';
    protected $ID = NULL;

    # Create our user session object; there's one assigned to each user session.
    public function __construct() {
        $this->parameters = array_fill_keys(array(
            'user_id',
            'password_reset_code',
            'date_added',
            'request_ip',
        ), NULL);
    }
}