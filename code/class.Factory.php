<?php

/*
 * Generic methods for object handling
 *
 * A lot of this is going to use this class as a base, so I want methods that will deal effectively with loading
 * objects, adding new ones, and so on.
 *
 * To create a new class, declare your table name and the primary key. Anything else that you're going to want to
 * maintain needs to be declared in $parameters . Those are the only attributes you'll be able to use - I've
 * over-ridden the default __get and __set to use that array instead of creating attributes on the fly.
 *
 * The constants at the head of the file define the state of this object, and by extension, what we want to do with
 * it. FACTORY_STATUS_ defines the state of the object - it's either CLEAN (no changes have been made); UPDATE
 * (changes have been made) or DELETE (remove it from the database).
 * # FACTORY_SOURCE_ is related, and only used by UPDATE. USER means that it's been created by a user action; while
 * DB means it originates from the database. They're used to define which SQL is used to write the changes back to
 * the database.
 */

define('FACTORY_STATUS_CLEAN', 1);          # No changes have been made to this object
define('FACTORY_STATUS_UPDATE', 2);         # Object has been updated.
define('FACTORY_STATUS_DELETE', 3);

define('FACTORY_SOURCE_USER', 1);
define('FACTORY_SOURCE_DB', 2);

abstract class Factory {
	protected $parameters = array();

	protected $table_name;				// the name of the table this object is taken from
	protected $primary_key;				// the name of the primary key field from the database used by this object
	protected $ID;						// the value of the primary key for this instance

    protected $_status = FACTORY_STATUS_CLEAN;
    protected $_source = FACTORY_SOURCE_USER;

    # Over-ridden default __get method. Looks up the passed in key in $parameters.
    final public function __get($field) {
		if (isset($this->parameters[$field])) {
			return $this->parameters[$field];
		}
		return (NULL);
	}

	# Over-ridden default __set method. Sets the passed in key in $parameters, and marks this object as needing
    # updating if the value has changed.
	final public function __set($field, $value) {
	    $updated = FALSE;

        if ($this->parameters[$field] !== $value) {
            $this->parameters[$field] = $value;

            $updated = TRUE;
        }

        if ($updated) {
            $this->_status = FACTORY_STATUS_UPDATE;
        }

        return ($value);
	}

	# Fetch a list of the parameter key names for this object.
	final public function getAttributes() {
        return (array_keys($this->parameters));
    }

    # Set the ID of this object.
    public function setID($id) {
        $this->ID = $id;
    }

    public function getID() {
    	return ($this->ID);
	}

    # Write this object to the database. Used when the source is FACTORY_SOURCE_USER - there's no current record
    # in the database, so we need to INSERT this one.
    #
    # Returns the ID of the record that's just been added.
	final public function addToDB () {
        $DB = DB::getInstance();

        # We're using a prepared statement to insert the data. We need to generate the statement dynamically.
        #
        # We step through the $parameters array, and generate an array of values for each part of the SQL that
        # is dynamic - the fieldnames, placeholders for the values, and the values themselves.

        $fields = array();          # gets a list of fieldnames
        $placeholders = array();    # gets a list of placeholders. Literally just a list of ?s
        $values = array();          # gets a list of values.

		foreach ($this->getAttributes() as $field) {
			$fields[] = $field;
			$placeholders[] = '?';
			$values[] = $this->$field;
		}

		# Now, create the SQL, imploding the fieldnames and the placeholders in the right place.
		$the_sql = 'INSERT INTO ' . $this->table_name . ' (';
		$the_sql .= implode(', ', $fields);
		$the_sql .= ') VALUES (';
		$the_sql .= implode(', ', $placeholders);
        $the_sql .= ')';

        # Prepare and execute the statement, passing the in the $values array as the parameters.
		$stmt = $DB->prepare($the_sql);
		if (!$stmt->execute($values)) {
		    var_dump($the_sql);
		    var_dump($values);
		    var_dump($stmt->errorInfo());
        }

        # Finally, get the ID of the new record. Set that to our primary key; set this object's status to CLEAN; and
        # return the ID
		$insert_id = $DB->lastInsertId();

		$this->ID = $insert_id;

        $this->_status = FACTORY_STATUS_CLEAN;

        return ($insert_id);
	}

	# Similar logic to addToDB above, but using UPDATE instead of INSERT. This is used for objects that were originally
    # retrieved from the database.
    #
    # One other difference is that this accepts an optional array. If set, the UPDATE will update the fields and
    # values in that array only; if not set, the entire object is updated.
    #
    # Returns the number of rows updated.
	final public function updateDB ($user_data = array()) {
	    $DB = DB::getInstance();

		$fields = array();
		$values = array();

		# If there's nothing in $user_data, we want to save the whole object, so use all the object's attributes
		if (count($user_data) == 0) {
			$user_data = $this->getAttributes();
		}

		# Generate the lists we're going to use in the SQL call.
        # Note that we only need two fields here - because it's an update, I'm just catting the placeholder after
        # the fieldname.
		foreach ($user_data as $field) {
			$fields[] = $field . '=?';
			$values[] = $this->$field;
		}

        # We're putting this objects ID into the values array, too. This gets mapped to the placeholder that's in the
        # WHERE clause.
		$values[] = $this->ID;

		$the_sql = 'UPDATE ' . $this->table_name . ' SET ';
		$the_sql .= implode($fields, ', ') . ' ';
		$the_sql .= 'WHERE ' . $this->primary_key . '=?';

		$stmt = $DB->prepare($the_sql);
		$stmt->execute($values);

		$row_count = $stmt->rowCount();

		# Set this object's status to CLEAN
        $this->_status = FACTORY_STATUS_CLEAN;

		return ($row_count);
	}

	# Load a record from the database into this object.
    #
    # By default, uses the primary key to locate the record. However, it will accept an optional array of
    # parameters - if that is set, then we use those to generate the WHERE clause instead.
	final public function loadFromDB ($search_parameters = []) {
        $DB = DB::getInstance();

	    # The arrays we use to generate the SQL query
        $database_parameter_names = [];
        $database_parameter_values = [];

        if ($search_parameters == []) {
            # We don't have any parameters passed in, so we're going to use the primary key.
            $database_parameter_names[] = $this->primary_key . '=?';
            $database_parameter_values[] = $this->ID;
        } else {
            # We do have some search parameters passed in; so use for our query instead.
            foreach ($search_parameters as $key => $value) {
                $database_parameter_names[] = $key . '=?';
                $database_parameter_values[] = $value;
            }
        }

        # This is the list of columns we're retrieving from the database. It's the fieldnames from our
        # object's parameters, plus the name of the primary key.
        $database_column_names = array_merge($this->getAttributes(), [$this->primary_key]);

        $the_sql = 'SELECT ';
		$the_sql .= implode($database_column_names, ', ') . ' ';
		$the_sql .= 'FROM ';
		$the_sql .= $this->table_name . ' ';
		$the_sql .= 'WHERE ';
		$the_sql .= implode(', ', $database_parameter_names);

		$stmt = $DB->prepare($the_sql);
		$stmt->execute($database_parameter_values);

        # If we can match a record, load all the fields into this object's attributes, and return the primary key.
        if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			foreach (array_keys($this->parameters) as $field) {
				$this->$field = $row[$field];
			}

			# The primary key isn't in the parameter array, so it needs explicitly setting. Also remember that every
            # object has its primary key set to ID, and this library deals with converting that to the correct value.
			$this->ID = $row[$this->primary_key];

            # Set the status and source correctly so it's flagged as a clean record from the database.
            $this->_source = FACTORY_SOURCE_DB;
            $this->_status = FACTORY_STATUS_CLEAN;

            return $this->ID;
		}

        return FALSE;
	}

	# Delete a record from the database.
    #
    # Takes an option array of parameters to use in the query; if not set, defaults to using the ID of the record.
	final public function deleteFromDB ($search_parameters = []) {
        $DB = DB::getInstance();

        $database_parameter_names = [];
        $database_parameter_values = [];

        if ($search_parameters == []) {
            # We don't have any parameters passed in, so we're going to use the primary key.
            $database_parameter_names[] = $this->primary_key . '=?';
            $database_parameter_values[] = $this->ID;
        } else {
            # We do have some search parameters passed in; so use for our query instead.
            foreach ($search_parameters as $key => $value) {
                $database_parameter_names[] = $key . '=?';
                $database_parameter_values[] = $value;
            }
        }

        $the_sql = 'DELETE FROM ';
		$the_sql .= $this->table_name . ' ';
		$the_sql .= 'WHERE ';
		$the_sql .= implode(', ', $database_parameter_names);

		$stmt = $DB->prepare($the_sql);
		$stmt->execute($database_parameter_values);

		$row_count = $stmt->rowCount();

        $this->_status = FACTORY_STATUS_CLEAN;

        return ($row_count);
	}

    # We want to save an object; this function calls the correct function, depending on where the object originally
    # came from.
	final public function saveChanges() {
        switch ($this->_source) {
            case FACTORY_SOURCE_USER:       $this->addToDB();
                                            break;
            case FACTORY_SOURCE_DB:         $this->updateDB();
                                            break;
        }
    }

    # Over-ride the default destructor
    # Make sure that the object we're destroying can be safely destroyed - ensure that changes have been saved.
	final public function __destruct() {

	    switch ($this->_status) {
            case FACTORY_STATUS_DELETE:     $this->deleteFromDB();
                                            break;
            case FACTORY_STATUS_UPDATE:     $this->saveChanges();
                                            break;
        }
	}

	final public function __toString() {
	    $ret_val = '';
	    foreach ($this->getAttributes() as $key => $value) {
	        $ret_val .= $key . '->' . $this->parameters[$key] . "</br>\n";
        }

        return ($ret_val);
    }
}

?>
