<?php

/*
 * This is the generic order object.
 *
 * When a session is started on the site, we instantiate an order and assign it to the user.
 * The status of the order at that point is Basket, because it's still the user's shopping basket.
 *
 * That status will change as the order goes through the process.
 *
 * An order is linked to a user_id
 * An order can have 0 or more OrderLines
 */

final class Order extends Factory {
    protected $table_name = 'order';
    protected $primary_key = 'order_id';
    protected $ID = NULL;

    private static $instance = NULL;

    public static function getInstance() {
        if (self::$instance == NULL) {
            self::$instance = new Order();
        }

        return self::$instance;
    }

    private function __construct() {
        $this->parameters = array_fill_keys(array(
            'order_status',
        ), '');
    }

    public function get_order_status() {
        return $this->order_status;
    }

    public function set_order_status($order_status) {
        $this->order_status = $order_status;
    }
}
