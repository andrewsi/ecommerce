<?php

final class SessionStatus {
    protected $table_name = 'session_status';
    protected $primary_key = 'session_status_id';
    protected $ID = NULL;

    protected $status_array = NULL;

    public function __construct($DB) {
        if (is_null($this->status_array)) {
            $this->instance($DB);
        }
    }

    private function instance($DB) {
        $this->status_array = Array();

        $the_sql = "SELECT session_status_id, session_status FROM session_status";

        $result_set = $DB->execute($the_sql);

        while ($row = $result_set->fetch_array()) {
            $this->status_array[$row['session_status_id']] = $row['session_status'];
        }

        return 1;
    }

    public function lookup_status($status_id) {
        if (isset($this->status_array[$status_id])) {
            return $this->status_array[$status_id];
        }

        return NULL;
    }

}
