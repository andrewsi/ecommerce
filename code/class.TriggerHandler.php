<?php

class triggerHandler {
	/*
	 *	I'm setting up an entirely static class to deal with triggers.
	 *	
	 *	This lets me deal with triggers internally using the advantages of a class design, and I can also 
	 *	add in wrappers that manipulate the contents - so you can work with the class without having to
	 *	mess around with the actual implementation details. 
	 *
	 *	It also means that using it means you can just call the wrapper function, rather than knowing the 
	 *	technical details of how it works.
	 */

	private static $triggers = array();

	public static function addTrigger($triggerName, $priority, $functionName, $parameters) {
		// Create a new trigger object to store the details 

		$trigger = new Trigger ($functionName, $parameters);

		// Add a new trigger to the array

		if (! isset(self::$triggers[$triggerName])) {
			// If there are no triggers for this event, then create an empty array for it

			self::$triggers[$triggerName] = array();
		}

		if (! isset(self::$triggers[$triggerName][$priority])) {
			// If there are no triggers for this event for this priority, add an empty array for it

			self::$triggers[$triggerName][$priority] = array();
		}

		// Add this trigger to the list
		self::$triggers[$triggerName][$priority][] = $trigger;
	}

	public static function fetchTriggers($triggerName) {
		// Return all the functions for this trigger; or an empty array if there are none

		$retVal = array();

		if (isset(self::$triggers[$triggerName])) {
			$retVal = self::$triggers[$triggerName];
		}

		return $retVal;
	}
}
