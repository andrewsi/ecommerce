<?php

class Config {
    private static $parameters = array();
    private static $_instance;

    public static function getInstance() {
        if (! self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    private function __construct() {
        $DB = DB::getInstance();

        $theSQL = 'SELECT config_name, config_value FROM config';

        $resSet = $DB->query($theSQL);

        while ($row = $resSet->fetch()) {
            self::$parameters[$row['config_name']] = $row['config_value'];
        }

    }

    public function __get($fieldname) {
        if (array_key_exists($fieldname, self::$parameters)) {
            return self::$parameters[$fieldname];
        }

        return '';
    }

    public function getConfigParameters() {
        return (array_keys(self::$parameters));
    }

}
