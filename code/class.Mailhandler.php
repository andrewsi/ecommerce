<?php

use PHPMAILER\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require FILEROOT . 'libraries/PHPMailer/Exception.php';
require FILEROOT . 'libraries/PHPMailer/PHPMailer.php';
require FILEROOT . 'libraries/PHPMailer/SMTP.php';

class MailHandler {
    private $mail_root = '';


    public function __construct() {
        $this->mail_root = THEMEROOT . '_email_templates/';
    }

    public function send_email($template, $mail_to, $parameters, $subject) {
        $template_name = $this->mail_root . $template . ".email";

        if (! file_exists($template_name)) {
            log_error("Mail template " . $template_name . " not found");
        }

        $config = Config::getInstance();

        foreach ($config->getConfigParameters() as $config_key) {
            $parameters['config_' . $config_key] = $config->$config_key ;
        }

        $email_text = file_get_contents($template_name);

        preg_match_all('/--(.*?)?--/', $email_text, $matches, PREG_SET_ORDER);

        foreach ($matches as $match) {
            $email_text = str_replace($match[0], $parameters[$match[1]], $email_text);
        }

        $parameters = [
            'from_email'    =>  $config->site_email,
            'from_name'     =>  $config->website,
            'to_email'      =>  $mail_to,
            'subject'       =>  $subject,
            'content'       =>  $email_text,
        ];

        $this->send_mail($parameters);

        return 1;
    }

    private function send_mail($parameters) {
        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        try {
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
            $mail->Port = 587;                                    // TCP port to connect to
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'mortdieu@gmail.com';                 // SMTP username
            $mail->Password = 'Ncc1701a!';                           // SMTP password

            //Server settings
            $mail->Mailer = "smtp";

            //Recipients
            $mail->setFrom($parameters['from_email'], $parameters['from_name']);
            $mail->addAddress($parameters['to_email']);     // Add a recipient

            //Content
            $mail->isHTML(false);                                  // Set email format to HTML
            $mail->Subject = $parameters['subject'];
            $mail->Body    = $parameters['content'];

            $mail->send();
        } catch (Exception $e) {
            log_error('Email could not be sent: ' . $mail->ErrorInfo, TRUE);
        }
    }
}