<?php

/*
 *	Code that deals with retrieving navigation information from the database
 *
 *	You can change the formatting of your navigation items by setting items in the optional parameters. 
 *	This is stored internally in a hash; currently supported ones are:
 *
 *		entry_text_before		Contents printed out directly before the text of an entry
 *		entry_text_after		Contents printed out directly after the text of an entry
 *
 *	These will obviously be theme-dependant, so they should be set inside your theme. Currently I'm using the functions.php file for that; though that may change if I come up with a 
 *	neater option
 */

function setNavigationParameters ($parameters) {
	/*
	 * Wrapper for the NavigationParameter object
	 * Sets the parameters that the Navigation code will use when printing out the navigation menu
	 */

	NavigationParameters::setParameters($parameters);
}

function getNavigationParameter ($parameter) {
	/*
	 * Wrapper for the NavigationParameter object
	 * Returns a single parameter from the navigation paramaters
	 */

	return NavigationParameters::getParameter($parameter);
}

function loadCategories() {
	/*
	 *	Fetches the list of categories from the database.
	 *	Returns an array of Navigation objects, containing the whole category tree
	 */

	global $DB;

	$theSQL = 'SELECT category_id, category_name, parent_id, ordering FROM category ORDER by parent_id, ordering';

	$resSet = $DB->query($theSQL);

	$navigation = array();

	while ($row = $resSet->fetch(PDO::FETCH_ASSOC)) {
		$navigationEntry = new Navigation ($row);

		$navigation[$row['category_id']] = $navigationEntry;
	}

	// At this point, we've got a basic array of navigation objects. We now need to go through that list, 
	// and order it so that the hierarchy is correct.

	$retVal = array();

	foreach ($navigation as $thisCategory) {
		if ($thisCategory->getParentID() === null) {
			// This is a top-level category, so add it straight to the return
			$retVal[] = $thisCategory;
		} else {
			// This is a child category, so add it to its parent category as a child
			
			$navigation[$thisCategory->getParentID()]->addChild($thisCategory);
		}
	}

	return $retVal;
}

function printMainNav($whereAmI = 0) {
	/*
	 *	Print out the navigation
	 *	
	 *	Simply a wrapper around the mainNav function
	 */

	echo mainNav($whereAmI);
}

function mainNav($whereAmI = 0) {
	/*
	 *	Generates the main navigation bar
	 *
	 *	returns the finished 
	 */

	global $DB;

	$retVal = '';

	$navigation = loadCategories();

	$retVal .= generateNavigation($navigation, $whereAmI);

	return $retVal;
}

function generateNavigation ($navigationArray, $whereAmI) {
	/*
	 *	Do the work of generating the navigation code.
	 *
	 *	Step through the neatly ordered array of navigation objects; create the code as we go.
	 *
	 */

	$retVal = '';

	$retVal .= getNavigationParameter('menu_start');
	foreach ($navigationArray as $navigationItem) {
		$retVal .= getNavigationParameter('item_start');

		if ($navigationItem->getCategoryID() != $whereAmI) {
			$retVal .= '<a href="/?categoryID=' . $navigationItem->getCategoryID() . '">';
		}

		$retVal .= getNavigationParameter('entry_text_before');
		$retVal .= $navigationItem->getCategoryName();
		$retVal .= getNavigationParameter('entry_text_after');

		if ($navigationItem->getCategoryID() != $whereAmI) {
			$retVal .= '</a>';
		}

		if (count($navigationItem->getChildren()) > 0) {
			$retVal .= generateNavigation($navigationItem->getChildren(), $whereAmI);
		}

		$retVal .= getNavigationParameter('item_end');
	}
	$retVal .= getNavigationParameter('menu_end');

	return $retVal;
}
