<?php

/*
 * This is a one of the objects that makes up a Product; it holds the values that represent an object that are
 * going to change. When you change the price of an existing Product, you're going to create a new ProductInstance
 * that contains those new details, that will become the new version of this product.
 */

final class ProductInstance extends Factory {
    protected $table_name = 'product_instance';
    protected $primary_key = 'product_instance_id';
    protected $ID = null;

    protected $price = 0;
    protected $start_date = 0;
    protected $end_date = 0;

    public function __construct($DB, $price, $start_date=NULL, $end_date=NULL) {
        $this->DB = $DB;

        $this->price = $price;

        if (!is_null($start_date)) {
            $this->start_date = $start_date;
        } else {
            $this->start_date = time();
        }

        if (! is_null($end_date)) {
            $this->end_date = $end_date;
        }
    }

}