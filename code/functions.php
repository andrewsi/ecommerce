<?php

/*
 * This is a comparison file to functions_core.
 *
 * The difference is that functions here can be over-written by theme-defined functions; so anything in here needs to
 * be wrapped in a call to function_exists
 */

# Create a random text string; used to generated a token used to activate an account, and to re-set passwords.
#
# Note that I'm mixing digits and characters; and characters are randomly in upper or lower case. So I'm
# explicitly skipping numbers and letters that look like one another.
if (! function_exists('generate_random_text')) {
    function generate_random_text($length = 20) {
        $letters = 'abcdefghjkmnopqrstuvwxyz';
        $numbers = '23456789';

        $random_string = '';

        for ($i = 0; $i < $length; $i++) {
            if (rand(0, 1) == 0) {
                $random_string .= $numbers[rand(0, strlen($numbers)-1)];
            } else {
                $case = rand(0, 1);
                if ($case == 0) {
                    $random_string .= strtoupper($letters[rand(0, strlen($letters) - 1)]);
                } else {
                    $random_string .= strtolower($letters[rand(0, strlen($letters) - 1)]);
                }
            }
        }

        return ($random_string);
    }
}

# Rules to check that an account username is well formed.
#
# Can be over-ridden.
#
# Returns either a text description of why the username is not valid; or an empty string.
if (! function_exists('account_username_rules')) {
    function account_username_rules($username) {
        if (strlen($username) < 8) {
            return ("Usernames must be at least 8 characters long");
        }

        return ('');
    }
}

# Rules to check that an account password is well formed.
# Can be over-ridden.
#
# Returns either a text description of why the password is not valid; or an empty string.
if (! function_exists('account_password_rules')) {
    function account_password_rules($password) {
        if (strlen($password) < 8) {
            return ("Passwords must be at least 8 characters long");
        }
        return ('');
    }
}