<?php

/*
 * This is a comparison file to functions
 *
 * These functions are always available, and should never be over-written by theme functions
 */

function log_error($error_message, $is_warning = FALSE) {
    $DB = DB::getInstance();

	$theSQL = 'INSERT INTO errors (error_message) VALUES (?)';

	$stmt = $DB->prepare($theSQL);
	$stmt->execute(array($error_message));

	error_page($error_message);

	if (! $is_warning) {
        exit;
    }
}

# Generate a UUID and return it.
function get_uuid() {
    /*
     * Taken from:
     * http://guid.us/GUID/PHP
     */
    mt_srand((double)microtime() * 10000);
    $char_id = strtoupper(md5(uniqid(rand(), true)));
    $hyphen = chr(45);// "-"
    $uuid = substr($char_id, 0, 8) . $hyphen
            . substr($char_id, 8, 4) . $hyphen
            . substr($char_id, 12, 4) . $hyphen
            . substr($char_id, 16, 4) . $hyphen
            . substr($char_id, 20, 12);

    return $uuid;
}

# Go through a table, and load its contents into constants for each of use.
# Takes a table name as a parameter; expects the fields it's loading to have the tablename as the root of their
# name, with prefixes of _status as the text and _status_id as the value.
#
# Used to load the list_xx_status tables
function load_table_constants($tablename) {
    $the_sql = 'SELECT '
                . $tablename . '_status_id AS status_id, '
                . $tablename . '_status AS status '
                . 'FROM list_' . $tablename . '_status ';

    $DB = DB::getInstance();

    $stmt = $DB->prepare($the_sql);
    $stmt->execute();

    # Create a const for each entry in the table.
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        define(strtoupper($tablename . '_' . $row['status']), $row['status_id']);
    }
}

# Accepts an array of tablenames; and calls load_table_constants on each of them.
function load_constants($table_names) {
    foreach ($table_names as $table) {
        load_table_constants($table);
    }
}

# Takes a fieldname and an array. If that fieldname is in the array, return it. Otherwise, return the default, which
# is an empty string unless otherwise specified.
function get_safe ($fieldname, $dict, $default='') {
    if (isset($dict[$fieldname])) {
        return ($dict[$fieldname]);
    }
    return ($default);
}

# A wrapper around get_safe, above; prints the output returned to it.
function print_safe($fieldname, $dict) {
    print(get_safe($fieldname, $dict));
}

# Accepts an array of parameters; and returns a link with those parameters as a query string.
function get_link_url($params) {
    $link = $_SERVER['PHP_SELF'];

    $link .= '?' . http_build_query($params);

    return ($link);
}

# Wrapper around the above, to print out the link directly.
function print_link_url($params) {
    print get_link_url($params);
}

# returns TRUE if there's a field called submitted in $_REQUEST
function is_form_submitted() {
    if (in_array('submitted', array_keys($_REQUEST))) {
        return TRUE;
    }

    return FALSE;
}

# Wrapper to generate error or information messages; checks to see if there's an entry in the dict with those details.
# If there is, it prints it out in a <p> tag, with the message type set as the class.
function print_message($message_type, $message) {
    if (strlen($message) > 0) {
        print("<p class='{$message_type}'>");
        print($message);
        print("</p>");
    }
}

function error_page($error_message) {
    $config = Config::getInstance();

    $error_file = THEMEROOT . '/error.php';

    if (! file_exists($error_file)) {
        echo $error_message . '<br />';
        echo 'And no error page was found to handle the error';
    } else {
        require($error_file);
    }
}

function _print_page($page_name) {
    $config = Config::getInstance();
    $session = UserSession::getInstance();

    $header_file = THEMEROOT . 'header.php';
    $footer_file = THEMEROOT . 'footer.php';

    require ($header_file);

    runTrigger('bodyMain');

    require ($page_name);
    require ($footer_file);

    return 1;
}
