<?php

/*
 * Code to deal with account creation.
 */

# Check the form submission to make sure it's all entered, and that we're not trying to create a duplicate account.
function account_create_validate() {
    # Load up the UserSession instance - we'll be storing errors in there, if there are any.
    $session = UserSession::getInstance();

    # The first few callas are to make sure that we have data set in the different fields.
    if (($username = get_safe('username', $_REQUEST)) === '') {
        $session->set_error_message("Please enter a username");
        return FALSE;
    }

    if (($email = get_safe('email', $_REQUEST)) === '') {
        $session->set_error_message("Please enter an email address");
        return FALSE;
    }

    if (($password_1 = get_safe('password_1', $_REQUEST)) === '') {
        $session->set_error_message("Please enter a password");
        return FALSE;
    }

    if (($password_2 = get_safe('password_2', $_REQUEST)) === '') {
        $session->set_error_message("Please enter your password again");
        return FALSE;
    }

    # Now, check that the username and password meet the standards defined by the theme.
    #
    # These functions can be over-ridden. They should return either a string explaining why the username or password
    # is invalid; or an empty string.
    $username_error = account_username_rules($username);
    if (strlen($username_error) > 0) {
        $session->set_error_message($username_error);
        return FALSE;
    }

    $password_error = account_password_rules($password_1);
    if (strlen($password_error) > 0) {
        $session->set_error_message($password_error);
        return FALSE;
    }

    # Check that both passwords match
    if ($password_1 !== $password_2) {
        $session->set_error_message("Your passwords do not match");
        return FALSE;
    }

    # Ensure that the username is unique
    if (account_check_username_exists($username)) {
        $session->set_error_message("An account with that username already exists");
        return FALSE;
    }

    # Ensure that the email is unique.
    if (account_check_email_exists($email)) {
        $session->set_error_message("An account with that email address already exists");
        return FALSE;
    }

    return TRUE;
}

# Add the details of the new account; and add the validation code to it.
function account_create_add_validation_code() {
    $validation_code = generate_random_text();

    # Create the UserSession and User instances.
    $session = UserSession::getInstance();
    $user = User::getInstance();

    # Populate our new User with the submitted details.
    $user->username = $_REQUEST['username'];
    $user->email_address = $_REQUEST['email'];
    $user->password = password_hash($_REQUEST['password_1'], PASSWORD_DEFAULT);

    # Set the user status to PENDING
    $user->user_status_id = USER_PENDING;

    # Set the validation code.
    $user->validation_code = $validation_code;

    # Write everything to the database.
    #
    # I generally don't explicitly save anything, but in this case, it's necessary - we need the ID of our new user
    #
    $user_id = $user->addToDB();

    # Now, we're going to send the Welcome email.
    #
    # The first thing we do is create an array, with the specific parameters that we're going to be including in
    # the email; in this case, it's just the validation code, and the rest will be populated from the config file.
    $email_parameters = [
        'validate_code'     =>  generate_random_text(),
    ];

    # Send the email.
    $mail_handler = new MailHandler();
    $mail_handler->send_email('welcome', $user->email_address, $email_parameters, "Welcome!");

    # Finally, update the session, so it's pointing at the new user, and set that session's status to LOGGED IN
    $session->user_id = $user_id;
    $session->set_login_status(SESSION_LOGGED_IN);

    return TRUE;
}

# Control code for creating an account.

# Get the UserSession instance.
$session = UserSession::getInstance();

# Set a default content file to display; if the form has been submitted, and the details pass validation, we'll
# switch to using the Thanks page
$create_new_account_file = FILEROOT . 'plugins/themes/_inbuilt/account/create_content.php';
if (is_form_submitted()) {
    if (account_create_validate()) {
        account_create_add_validation_code();
        $create_new_account_file = FILEROOT . 'plugins/themes/_inbuilt/account/create_thanks.php';
    }
}

return _print_page($create_new_account_file);

