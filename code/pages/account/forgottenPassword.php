<?php

/*
 * Control code for password reset code.
 *
 * When you click on this, we're going to send a validation code to your email address.
 *
 * Clicking on that code will take you to a page where you can reset your password; we need to store those codes,
 * along with a timeout.
 *
 * This code deals with sending out the email, and setting the details in the database. The password reset will be
 * done elsewhere.
 */

# Ensure that an email address has been entered.
function account_password_reminder_validate() {
    $session = UserSession::getInstance();

    if (($email = get_safe('email', $_REQUEST)) === '') {
        $session->set_error_message("Please enter your email address");
        return FALSE;
    }

    return TRUE;
}

# Send the validation code to the entered email address
function account_password_reminder_doEmail() {
    $email = $_REQUEST['email'];

    if (! account_check_email_exists($email)) {
        $user = User::getInstance();
        if ($user->loadFromDB([ 'email_address' => $_REQUEST['email'] ])) {
            $validation_code = generate_random_text();

            $password_reset = new UserPasswordReset();

            $password_reset->user_id = $user->getID();
            $password_reset->password_reset_code = $validation_code;
            $password_reset->date_added = date('Y-m-d H:i:s', time());
            $password_reset->request_ip = $_SERVER['REMOTE_ADDR'];

            $email_parameters = [
                'reset_code' => $validation_code,
                'username' => $user->username,
                'email' => $user->email_address,
            ];

            $mail_handler = new MailHandler();
            $mail_handler->send_email('password_reset', $user->email_address, $email_parameters, "Your validation code");

            return TRUE;
        }
    }

    return FALSE;
}

# Create the UserSession instance
$session = UserSession::getInstance();

# Set the default content file to display. If the content is correct, we're going to display the Thanks page
$password_reminder_file = FILEROOT . 'plugins/themes/_inbuilt/account/forgotten_password_content.php';

if (is_form_submitted()) {
    if (account_password_reminder_validate()) {
        account_password_reminder_doEmail();
        $password_reminder_file = FILEROOT . 'plugins/themes/_inbuilt/account/forgotten_password_thanks.php';
    }
}

_print_page($password_reminder_file);
