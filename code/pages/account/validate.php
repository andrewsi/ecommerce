<?php

/*
 * Code to deal with a submitted user validation code.
 */

# Check that we've been given a validation code, and that it matches the logged in user's.
function account_validation_validate() {
    $user = User::getInstance();
    $session = UserSession::getInstance();

    if (($validation_code = get_safe('validation_code', $_REQUEST)) === '') {
        $session->set_error_message('Please enter your validation code');
        return FALSE;
    }

    if ($validation_code != $user->validation_code) {
        $session->set_error_message('Incorrect validation code');
        return FALSE;
    }

    return TRUE;
}

# Update the user so that they're set to LIVE
function account_validation_doValidation() {
    $user = User::getInstance();

    $user->user_status_id = USER_LIVE;

    return TRUE;
}

# Create the user instance
$session = UserSession::getInstance();

# Set a default content file; if the form is submitted, and the data is correct, validate the account and show the
# Thanks page instead.
$password_validation_file = FILEROOT . 'plugins/themes/_inbuilt/account/validation_content.php';
if (is_form_submitted()) {
    if (account_validation_validate()) {
        account_validation_doValidation();
        $password_validation_file = FILEROOT . 'plugins/themes/_inbuilt/account/validation_thanks.php';
    }
}

return _print_page($password_validation_file);
