<?php

/*
 * Perform the password reset action.
 *
 * This is a multi-stage process.
 *
 * Firstly, print up the initial screen, and take the email address and validation code.
 *
 * Then, check that those credentials are correct. If not, bounce to the previous screen. If so:
 *
 * Secondly, print up a password screen, and have the user input their new password (twice).
 *
 * Then, validate that input. If it doesn't validate for whatever reason, bounce to the previous screen. Otherwise:
 *
 * Print up the Thanks; and update User to use the new password.
 */

function password_reset_validate_code() {
    $session = UserSession::getInstance();

    if (($email = get_safe('email', $_REQUEST)) === '') {
        $session->set_error_message('Please enter your email');
        return FALSE;
    }

    if (($code = get_safe('code', $_REQUEST)) === '') {
        $session->set_error_message('Please enter your reset code');
        return FALSE;
    }

    $user = User::getInstance();

    if ($user->loadFromDB([ 'email_address' => $email])) {

        $DB = DB::getInstance();

        $the_sql = "
          SELECT 
            password_reset_code 
          FROM 
            user_password_reset 
          WHERE 
            user_id=:user_id
            AND password_reset_code=:reset_code 
          ORDER BY date_added DESC
          LIMIT 1 
          ";

        $stmt = $DB->prepare($the_sql);
        $stmt->bindValue(':user_id', $user->getID());
        $stmt->bindValue(':reset_code', $code);
        $stmt->execute();

        if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $session->user_id = $user->getID();

            return TRUE;
        }
    }

    print('5');
    return FALSE;
}

function password_reset_update_password_validate() {
    $session = UserSession::getInstance();

    if (($password_1 = get_safe('password_1', $_REQUEST)) === '') {
        $session->set_error_message('Please enter your password');
        return FALSE;
    }

    if (($password_2 = get_safe('password_2', $_REQUEST)) === '') {
        $session->set_error_message('Please enter your password again');
        return FALSE;
    }

    $password_error = account_password_rules($password_1);
    if (strlen($password_error) > 0) {
        $session->set_error_message($password_error);
        return FALSE;
    }

    return TRUE;
}

function password_resent_password_doUpdate() {
    $session = UserSession::getInstance();
    $user = User::getInstance();

    $user->loadFromDB([ "user_id" => $session->user_id ]);

    $user->password = password_hash($_REQUEST['password_1'], PASSWORD_DEFAULT);

    return TRUE;
}



$session = UserSession::getInstance();

$password_reminder_file = FILEROOT . 'plugins/themes/_inbuilt/account/password_reset_content.php';

$step = 0;
if (isset($_REQUEST['submitted'])) {
    $step = $_REQUEST['submitted'];
}

if ($step == 1) {
    # Parse email/code combination

    if (password_reset_validate_code()) {
        $password_reminder_file = FILEROOT . 'plugins/themes/_inbuilt/account/password_reset_new_password.php';
    }
} elseif ($step == 2) {
    # Reset password

    if (password_reset_update_password_validate()) {
        password_resent_password_doUpdate();
        $password_reminder_file = FILEROOT . 'plugins/themes/_inbuilt/account/password_reset_thanks.php';
    } else {
        $password_reminder_file = FILEROOT . 'plugins/themes/_inbuilt/account/password_reset_new_password.php';
    }
}

return _print_page($password_reminder_file);
