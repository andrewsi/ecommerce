<?php

/*
 * Control code for resending a validation code.
 *
 * TODO - do I need to change the logic here?
 * Either we don't need them to enter an email address (because they're logged in but not validated); or we need to use
 * the user that has that as their email address.
 */

# Check that we have an email address entered.
function account_resend_validation_validate() {
    if (($email = get_safe('email', $_REQUEST)) === '') {
        return ("Please enter your email address");
    }

    return ("");
}

# Do the work of resending the validation email.
function account_resend_validation_doEmail() {
    $email = $_REQUEST['email'];

    # Check that we have an account with this email address.
    if (! account_check_email_exists($email)) {
        $validation_code = generate_random_text();

        $session = UserSession::getInstance();

        # Load up the User
        $user = User::getInstance();
        $user->setID($session->user_id);
        $user->loadFromDB();

        # Set the validation code
        $user->validation_code = $validation_code;

        # Set the parameters for the email we're going to send
        $email_parameters = [
            'validate_code'     =>  $validation_code,
            'username'          =>  $user->username,
        ];

        $mail_handler = new MailHandler();
        $mail_handler->send_email('resend_validation_code', $user->email_address, $email_parameters, "Your validation code");

        return TRUE;
    }

    return FALSE;
}

$session = UserSession::getInstance();

$password_reminder_file = FILEROOT . 'plugins/themes/_inbuilt/account/resend_validation_content.php';
if (is_form_submitted()) {
    if (account_resend_validation_validate()) {
        account_resend_validation_doEmail();
        $password_reminder_file = FILEROOT . 'plugins/themes/_inbuilt/account/resend_validation_thanks.php';
    }
}

return _print_page($password_reminder_file);
