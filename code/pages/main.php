<?php

/*
 * This is the default page that appears if there is no valid PAGE parameter in the request.
 */

$config = Config::getInstance();

$main_file = THEMEROOT . '/main.php';

_print_page($main_file);
