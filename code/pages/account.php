<?php

/*
 * Main control code for all things account-related.
 */

# Check to see if we've got an account using this email
function account_check_email_exists($email) {
    $the_sql = "SELECT email FROM user WHERE email = :email";

    $DB = DB::getInstance();

    $stmt = $DB->prepare($the_sql);
    $stmt->bindValue(':email', $email);
    $stmt->execute();

    if ($stmt === FALSE) {
        return FALSE;
    }

    if ($stmt->rowCount() == 0) {
        return FALSE;
    }

    return TRUE;
}

# Check to see if we've got an account using this username.
function account_check_username_exists($username) {
    $the_sql = "SELECT username FROM user WHERE username = :username";

    $DB = DB::getInstance();

    $stmt = $DB->prepare($the_sql);
    $stmt->bindValue(':username', $username);
    $stmt->execute();

    if ($stmt === FALSE) {
        return 0;
    }

    if ($stmt->rowCount() == 0) {
        return 0;
    }

    return 1;
}

# Print out the account box.
#
# There are three possibilities for the content; we're either going to print out a login; a message saying that
# your account isn't validated; or a set of links for a validated, logged in account.
#
# There are two checks - is this session logged in? And if so, is the user account validated?
function account_print_account_box() {
    $session = UserSession::getInstance();

    $login_box_file = FILEROOT . 'plugins/themes/_inbuilt/account/login_box_content.php';

    if ($session->is_logged_in()) {
        $user = User::getInstance();

        if ($user->user_status_id == USER_LIVE) {
            $login_box_file = FILEROOT . 'plugins/themes/_inbuilt/account/login_box_logged_in.php';
        } elseif ($user->user_status_id == USER_PENDING) {
            $login_box_file = FILEROOT . 'plugins/themes/_inbuilt/account/login_box_pending.php';
        }
    }

    require($login_box_file);
}

# This code is run before content is displayed, and ensures that UserSession is created appropriately.
function check_login() {
    # Generate a default UUID for the session.
    $session_id = get_uuid();

    # Get our UserSession instance, and set it to a non-logged in, anonymous user.
    $session = UserSession::getInstance();

    $session->session_status_id = SESSION_LOGGED_OUT;
    $session->ip_address = $_SERVER['REMOTE_ADDR'];
    $session->session_id = $session_id;
    $session->user_id = 0;

    # Check and see if there's a login cookie set.
    if (isset($_COOKIE['session_id'])) {
        # There is!
        $DB = DB::getInstance();

        # We're going to be using that session's UUID instead.
        $session_id = $_COOKIE['session_id'];

        # Now, we're going to look the session_id up in the database.
        $the_sql = 'SELECT user_id FROM user_session WHERE session_id=:session_id';

        $stmt = $DB->prepare($the_sql);
        $stmt->bindValue(':session_id', $session_id);
        $stmt->execute();

        if ($stmt !== FALSE) {
            if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                # We have found the session in the database; we're going to load its details into
                # $session now.
                # This over-writes the default settings, and because we're using loadFromDB, it flags this as
                # not to be saved.
                $session->loadFromDB(['session_id' => $session_id]);
                $session->user_id = $row['user_id'];

                # Load the user connected to this session into User.
                $user = User::getInstance();
                $user->loadFromDB([ 'user_id' => $row['user_id'] ]);
            }
        }
    }

    setcookie('session_id', $session->session_id, time() + 60 * 60 * 24);

    # If someone clicked LogOut, then set their session appropriately
    if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'logOut') {
        $session->set_login_status(SESSION_LOGGED_OUT);
        return FALSE;
    }

    # See if someone has clicked the Log In button
    if (is_login_attempt()) {
        return validate_login_attempt();
    }

    return FALSE;
}

# Validate an attempt to log in - ensure that everything is filled in, and that the credentials are correct
function validate_login_attempt() {
    $session = UserSession::getInstance();

    if ((! in_array('username', array_keys($_REQUEST))) or (strlen($_REQUEST['username']) == 0)) {
        $session->set_error_message('Please enter your username');
        return FALSE;
    }

    if ((! in_array('password', array_keys($_REQUEST))) or (strlen($_REQUEST['password']) == 0)) {
        $session->set_error_message('Please enter your password');
        return FALSE;
    }

    if ($user_id = check_password($_REQUEST['username'], $_REQUEST['password'])) {
        set_logged_in($user_id);
        return TRUE;
    }

    $session->set_error_message('Username not found; or invalid password');
    return FALSE;
}

# If I'm logging in on this session, I want to make sure that any other session I might have had is invalidated.
function do_logout_other_sessions($user_id, $user_session_id) {
    $the_sql = "
    UPDATE
        user_session
    SET
        session_status_id = :session_logged_out
    WHERE
        user_id = :user_id 
        AND user_session_id != :user_session_id  
        AND session_status_id = :session_logged_in
    ";

    $DB = DB::getInstance();

    $stmt = $DB->prepare($the_sql);

    $stmt->bindValue(':session_logged_out', SESSION_SESSION_LOGOUT);
    $stmt->bindValue(':user_session_id', $user_session_id);
    $stmt->bindValue(':user_id', $user_id);
    $stmt->bindValue(':session_logged_in', SESSION_LOGGED_IN);

    $stmt->execute();
}

# Check and see if the Do Login button has been clicked.
function is_login_attempt() {
    if (get_safe('action', $_REQUEST) == 'doLogin') {
        return 1;
    }

    return 0;
}

# Set a user to be logged in.
# If this is called, then we're also going to log them out of any other session that they might have active
# at this point.
function set_logged_in($user_id) {
    $session = UserSession::getInstance();

    $session->session_status_id = SESSION_LOGGED_IN;
    $session->user_id = $user_id;

    do_logout_other_sessions($session->user_ID, $session->getID());
}

# Check the password that is passed in, against the contents of the database to see if it matches.
# Returns 0 if we can't find the user; or if password_verify doesn't match.
function check_password($username, $password) {
    $the_sql = 'SELECT user_id, password FROM user WHERE username=:username';

    $DB = DB::getInstance();

    $stmt = $DB->prepare($the_sql);
    $stmt->bindValue(':username', $username);
    $stmt->execute();

    if ($stmt === FALSE) {
        return 0;
    }

    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    # We have a password for this user; and that password is correct
    if (password_verify($password, $row['password'])) {
        return ($row['user_id']);
    }

    return 0;
}

function do_logout() {
    $session = UserSession::getInstance();

    $session->set_login_status(SESSION_LOGGED_OUT);
}


# This is used by Switchboard. Register the functions here to add extra actions to your page.
class PageAccount {
    public static function do_action() {
        $switchboard = Switchboard::getInstance();

        # Set up all the actions that this page can deal with..
        $switchboard->add_action('resendValidation');
        $switchboard->add_action('forgottenPassword');
        $switchboard->add_action('resetPassword');
        $switchboard->add_action('create');
        $switchboard->add_action('validate');

        if (isset($_REQUEST['action']) && $switchboard->action_exists($_REQUEST['action'])) {
            include(PAGEROOT . 'account/' . $_REQUEST['action'] . '.php');
        } else {
            include(PAGEROOT . 'account/main.php');
        }
    }
}

# Register the class that deals with this page.
$switchboard->register_class('account', 'PageAccount', 'do_action');
