<?php

/*
 * We're creating an instance of Basket whenever we load a page.
 *
 * It is related to Order, though as of the now, I'm not quite sure what that relationship is going to be.
 *
 * Basically - a Basket is linked to a UserSession, and _not_ a User. That is because when someone logs in, they're
 * going to get an empty UserSession - one that has a user_id of 0. If we have two people using the site, we'll have
 * two sessions with user_id=0, so I can't link this directly to User.
 */

final class Basket extends Factory {
    protected $table_name = 'basket';
    protected $primary_key = 'basket_id';
    protected $ID = NULL;

    private static $instance = NULL;

    public static function getInstance() {
        if (self::$instance == NULL) {
            self::$instance = new Basket();
        }

        return self::$instance;
    }

    private function __construct() {
        $this->parameters = array_fill_keys(array(
            'session_id',
        ), NULL);
    }

    public function setSessionID($session_id) {
        $this->session_id = $session_id;
    }
}