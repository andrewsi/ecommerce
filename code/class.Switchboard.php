<?php

/*
 *  This is a class to track where to direct a given request.
 *
 * I'm splitting the code into two sections - pages and actions. A page is something that gets its own, er, page -
 * so viewing a product, account management, and so on. An action is something that is done on that page - adding a
 * product to the basket, or creating an account.
 *
 * Usage:
 *
 * call add_page in loader to create the pages that we're going to be using. The values added here are the values
 * that will be included in the PAGE parameter inside URLs.
 *
 * If this page is called because its PAGE is set, then we're including a file in pages with the same name as the PAGE
 * parameter. That file is then entirely responsible for processing everything.
 *
 * call add_action in the subfile for each action that the page will deal with.
 *
 * I'm currently setting the actions inside the page's code, so it's only called once the code in the page is run; so
 * only the actions for one page will ever be set. That means I don't need to worry about a more complex data
 * structure to hold everything; and indeed this may well be overkill for what it's doing.
 */

final class Switchboard {
    private $pages = array();
    private $actions = array();
    private static $instance = NULL;

    private function __construct() {
        $this->pages = array();
    }

    public static function getInstance() {
        if (self::$instance == NULL) {
            self::$instance = new Switchboard();
        }

        return self::$instance;
    }

    public function register_class($page, $class, $method) {
        $this->pages[$page] = array($class, $method);

        return TRUE;
    }

    public function add_action($action) {
        $this->actions[] = $action;

        return TRUE;
    }

    public function page_exists($page) {
        return (in_array($page, $this->get_pages()));
    }

    public function action_exists($action) {
        return (in_array($action, $this->actions));
    }

    public function delete_page($page) {
        if ($this->page_exists($page)) {
            unset($this->pages[$page]);

            return TRUE;
        }

        return FALSE;
    }

    public function get_pages() {
        return (array_keys($this->pages));
    }

    public function get_values() {
        return (array_values($this->pages));
    }

    public function get_file($page) {
        if ($this->page_exists($page)) {
            return ($this->pages[$page]);
        }
        return FALSE;
    }
}