<?php

/*
 *	These are pieces of code that are run at a specific point when the page is being created.
 *
 *	These can be overridden by theme triggers
 *
 *	These can take arbitrary parameters; and should only ever return data. 
 *	If you need to echo something out, return it and echo in the calling function
 *
 *	Technically, it's a static object, and the functions here are wrappers around its methods, to make using it a lot less intimidating - 
 *	you can call a simple function, rather than know what the name of the trigger handling class is
 */

function setTrigger($triggerName, $priority, $functionName, $parameters = array()) {
	/*
	 * This is the wrapper that hides the implementation of the object.
	 *
	 * Takes four parameters:
	 * - trigger name (when to call this function)
	 * - priority (integer. non-ints are set to priority 10; negative integers are set to 1)
	 * - functionName
	 * - parameters (to be passed straight to the function)
	 *
	 */

	$priority = intval($priority);

	if ($priority == 0) {
		$priority = 10;
	} elseif ($priority < 0) {
		$priority = 1;
	}

	triggerHandler::addTrigger($triggerName, $priority, $functionName, $parameters);
}

function runTrigger ($triggerName) {
	// Fires the triggers for a specific event

	// Firstly, fetch the triggers set
	$triggers = triggerHandler::fetchTriggers($triggerName);

	if ($triggers) {
		// Fetch the list of priorities that have been set, and then order them to make sure they're run in 
		// the correct order.
		$priorities = array_keys($triggers);

		sort($priorities);

		// Step through the list of priorities; for each priority, get the functions set and run each 
		// of them.
		foreach ($priorities as $priority) {
			foreach ($triggers[$priority] as $trigger) {
				call_user_func ($trigger->getFunctionName(), $trigger->getParameters());
			}
		}
	}
}
