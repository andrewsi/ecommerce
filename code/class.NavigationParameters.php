<?php

/*
 *	Static class for storing navigation parameters
 *
 *	Includes a handful of default parameters, to generate a HTML menu using unordered lists. These can naturally be over-written as required.
 */

Class NavigationParameters extends Parameters {
	static public function initialize() {
		self::setParameter('menu_start', '<ul>');
		self::setParameter('menu_end', '</ul>');

		self::setParameter('item_start', '<li>');
		self::setParameter('item_end', '</li>');
	}
}

